package org.iteco.ems.backend.repository;

import org.iteco.ems.backend.entity.Employee;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
interface EmployeeRepository extends PagingAndSortingRepository<Employee,String> {
}
